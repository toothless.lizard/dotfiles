export ZSH="/usr/share/oh-my-zsh"
ZSH_THEME="robbyrussell"

HISTSIZE=999999
SAVEHIST=999999
HISTFILE="${ZDOTDIR:-$HOME}/zsh_history"

setopt extended_history   # Record timestamp of command in HISTFILE
setopt hist_ignore_dups   # Ignore duplicated commands history list
setopt share_history      # Save command history before exiting

# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )
# CASE_SENSITIVE="true"
# HYPHEN_INSENSITIVE="true"
# DISABLE_MAGIC_FUNCTIONS="true"
# DISABLE_LS_COLORS="true"
# DISABLE_AUTO_TITLE="true"
# ENABLE_CORRECTION="true"
# COMPLETION_WAITING_DOTS="true"
# DISABLE_UNTRACKED_FILES_DIRTY="true"
# HIST_STAMPS="mm/dd/yyyy"
# ZSH_CUSTOM=/path/to/new-custom-folder

plugins=(
	git
	archlinux
	rust
	colored-man-pages
)

source $ZSH/oh-my-zsh.sh
export MANPATH="/usr/local/man:$MANPATH"
export LANG=en_US.UTF-8

fpath+=$HOME/.zsh/pure
autoload -U promptinit; promptinit
prompt pure

export PATH="${PATH}:${HOME}/.local/bin/"
export NODE_PATH="${HOME}/.npm-packages/lib/node_modules:${NODE_PATH}"

[ -z "$NVM_DIR" ] && export NVM_DIR="$HOME/.nvm"
source /usr/share/nvm/nvm.sh
source /usr/share/nvm/bash_completion
source /usr/share/nvm/install-nvm-exec

source $HOME/.cargo/env
export RUSTC_WRAPPER=sccache

export MANPAGER="sh -c 'col -bx | bat -l man -p'"
export BAT_PAGER="less -RF"
export BAT_CONFIG_PATH="$HOME/.config/bat/config"

#To allow global package installations for the current user
#PATH="$HOME/.local/bin:$PATH"
#export npm_config_prefix="$HOME/.local"
#npm set prefix="$HOME/.local"


alias xi="sudo pacman -S"
alias xq="sudo pacman -Q"

alias vim="nvim"

batdiff() {
    git diff --name-only --diff-filter=d | xargs bat --diff
}

