(use-modules (gnu) 
	     (gnu system pam) 
	     (gnu packages version-control)
	     (gnu packages wget)
	     (gnu packages curl)
	     (gnu packages ssh)
	     (gnu packages web)
	     (gnu packages guile)
	     (gnu packages less)
	     (gnu packages dns)
	     (gnu packages networking)
	     (gnu packages ghostscript)
	     (gnu packages fontutils)
	     (gnu packages compression)
	     (gnu packages pkg-config)
	     (gnu packages pulseaudio)
	     (nongnu packages linux)
	     (guix utils)
	     (guix packages))

(use-service-modules 
  		desktop 
  		networking 
  		ssh 
  		xorg
		linux
  		nfs)

(use-package-modules 
  		admin
		gnuzilla 
		vim
		bash
		emacs 
		ncurses 
		fonts 
		xfce 
		guile
		guile-xyz
		emacs-xyz
		ssh 
		xorg 
		dns)

(define %sudoers-specification
  (plain-file "sudoers" "\
root ALL=(ALL) ALL
%wheel ALL=(ALL) NOPASSWD:ALL
	      "))

(operating-system
  (locale "en_US.utf8")
  (timezone "Europe/Moscow")
  (keyboard-layout (keyboard-layout "us,ru" #:options '("grp:alt_shift_toggle")))
  (host-name "doom")
  (sudoers-file %sudoers-specification)
  (users (cons* (user-account
                  (name "orb")
                  (comment "Orb")
                  (group "users")
                  (home-directory "/home/orb")
                  (supplementary-groups
                    '("wheel" "netdev" "audio" "video")))
                %base-user-accounts))
  (packages
    (append
      (list (specification->package "nss-certs") 
       xfce
       tcpdump
       wireshark
       guile-studio
       vim-full
       htop
       glibc-locales
       bash
       bash-completion
       git
       xset setxkbmap
       wget
       less
       pulseaudio
       xkeyboard-config
       curl
       pfetch
       neofetch
       ;; emacs set
       emacs emacs-guix guile-wisp emacs-geiser guile-bash emacs-ac-geiser
       ncurses guile-colorised
       ;; guile set
       guile-libyaml
       openssh
       ;; icecat set
       icecat font-dejavu
       ;; fonts
       libxfont
       fontconfig
       gs-fonts
       font-adobe-source-code-pro
       font-anonymous-pro
       font-anonymous-pro-minus
       font-awesome
       font-fira-code
       font-gnu-freefont
       font-ubuntu
       font-misc-misc
       font-terminus
       font-bitstream-vera
       )
      %base-packages))
  (services
    (append
      (list (service xfce-desktop-service-type)
            (service openssh-service-type)
            (set-xorg-configuration
              (xorg-configuration
                (keyboard-layout keyboard-layout))))
      %desktop-services))
  (bootloader
    (bootloader-configuration
      (bootloader grub-bootloader)
      (target "/dev/sda")
      (keyboard-layout keyboard-layout)))
  (mapped-devices
    (list (mapped-device
            (source
              (uuid "3119f7a2-faea-462f-bc72-92339361be1a"))
            (target "cryptroot")
            (type luks-device-mapping))))
  (file-systems
    (cons* (file-system
             (mount-point "/")
             (device "/dev/mapper/cryptroot")
             (type "ext4")
             (dependencies mapped-devices))
           %base-file-systems)))
